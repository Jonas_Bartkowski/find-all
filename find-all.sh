#!/bin/bash

#findall [-op] [-R] [-D] [-l lfile] [dir]

pnt () {
	if test "-$op" "$1" ; then
		printf '%s\n' "$1"
	else
		db "$1 (FALSE)"
	fi
}

db() {
	if [ "$dbg" -eq 0 ]; then
		printf '%s\n' "$1" 1>&2
	fi
}

wlk() (
	cd "$1"

	ls -A |
	while read fname; do
		apth="$PWD"/"$fname"
		pnt "$apth"
		if test -d "$fname" && [ "$2" = "true" ]; then
			dname="$fname"
			if test -r "$dname" ; then
				if test -x "$dname" ; then
					wlk "$dname"
				else
					printf 'execution permission denied for directory "%s"\n' "$apth" 1>&2
				fi
			else
				printf 'read permission denied for directory "%s"\n' "$apth" 1>&2
			fi
		fi
	done
)

dir="$PWD/"
op="f"
rec=1
lfile=""

dbg=1

while getopts l:bcdefgGhkLOprsStuwxRD opt
do
	case $opt in
    	R) rec=0;;
	   	l) lfile="$OPTARG";;
		D) dbg=0;;
		![bcdefgGhkLOprsStuwx]) ;;
       	*) op=$opt;;
   	esac
done

shift $((OPTIND-1))

if [ ! "$lfile" = "" ]; then
	exec 2>> "$lfile"
	printf "$(date +'%Y-%m-%d %H:%M:%S') $PWD $SHELL\n" 1>&2
fi

if [ "$#" -gt 0 ]; then
	dir="$1"
fi

db "op is \"$op\""
db "dbg is \"$dbg\""
db "dir is \"$dir\""

if [ ! "$lfile" = "" ]; then
	db "lfile is \"$lfile\""
else
	db 'lfile not set!'
fi

if test -d "$dir" ; then
	if [ "$rec" -eq 0 ]; then
		db 'Searching recursively...'
		wlk "$dir" true
	else
		db 'Searching non-recursively...'
		wlk "$dir" false
	fi
else
	printf 'Could not find directory "%s"!\n' "$dir" 1>&2
fi